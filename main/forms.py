from django import forms
from .models import Matkul
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class Input_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['name','dosen','sks']
        fields = '__all__'
    error_massages = {
        'required' : 'please type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'masukan matkul'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'masukan dosen'
    }
    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'masukan sks'
    }
    name = forms.CharField(label = ' ', required = True , max_length=27 , widget=forms.TextInput(attrs=input_attrs))
    dosen = forms.CharField(label = ' ', required = True , max_length=27 , widget=forms.TextInput(attrs=input_attrs1))
    sks = forms.CharField(label = ' ', required = True , max_length=27 , widget=forms.TextInput(attrs=input_attrs2))

class Delete_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['name']   
    error_massages = {
        'required' : 'please type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'delete matkul'
    }
    name = forms.CharField(label = ' ', required = True , max_length=27 , widget=forms.TextInput(attrs=input_attrs))

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        