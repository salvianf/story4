from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.
class Matkul(models.Model):
    name = models.CharField(max_length=30)
    dosen =  models.CharField(max_length=30)
    sks = models.CharField( max_length=3)

class Post(models.Model):
    author = models.ForeignKey(Matkul, on_delete= models.CASCADE)
    content = models.CharField(max_length = 125)
    published_date = models.DateTimeField(default = timezone.now)
