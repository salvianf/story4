const accordion = document.getElementsByClassName('accordion');
for(i=0; i<accordion.length; i++){
    accordion[i].addEventListener('click' , function(){
        this.classList.toggle('active')
    })
}
function toggle_visibility(id) {
    var e = document.getElementById(id);
    var select = id;

    if(e.style.display == 'block')
        e.style.display = 'none';
        
    else
        e.style.display = 'block';
}
function hidetoggle_visibility(id){
    var e = document.getElementById(id);
    var select = id;
    e.style.display = 'none';
}
$(document).ready(function(){
    $('.nav_btn').click(function(){
        $('.mobile_nav_items').toggleClass('active');
    });
});


$(".move-up").click(function() {
    var select_content = $(this).parent().parent();
    select_content.prev().insertAfter(select_content);
})

$(".move-down").click(function() {
    var select_content = $(this).parent().parent();
    select_content.next().insertBefore(select_content);
})

$( function() {
    $( "#accordion" ).accordion({
        collapsible:true,
        event:"click",
        active:0,
        highStyle:true
    });
  } );