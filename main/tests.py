from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from main.models import Matkul


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)
    def test_template_main(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'base.html')

    def test_root_home_exists(self):
        response = self.client.get('/home')
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
         response = self.client.get('/home')
         self.assertTemplateUsed(response, 'base.html')

    def test_root_delete_exists(self):
        response = self.client.get('/delete')
        self.assertEqual(response.status_code, 200)

    def test_template_delete(self):
         response = self.client.get('/delete')
         self.assertTemplateUsed(response, 'base.html')

    def test_root_login_exists(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_root_register_exists(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_root_cari_exists(self):
        response = self.client.get('/cari')
        self.assertEqual(response.status_code, 200)

    def test_template_cari(self):
         response = self.client.get('/cari')
         self.assertTemplateUsed(response, 'base.html')



    # def test_view_response(self):
    #     response = self.client.get('/')
    #     isi_html = response.content.decode('utf8')

    #     self.assertIn("S", isi_html)
    #     self.assertIn("ALVI", isi_html)
    #     self.assertIn("AN", isi_html)
    #     self.assertIn("Back", isi_html)
    #     self.assertIn("DELETE MATKUL", isi_html)
    #     self.assertIn("ADD MATKUL", isi_html)
    #     self.assertIn("Salvian AF", isi_html)
    #     self.assertIn("Profile", isi_html)
    #     self.assertIn("Background Study", isi_html)
    #     self.assertIn("Experience", isi_html)
    #     self.assertIn("Schedule", isi_html)
    #     self.assertIn("Interest", isi_html)
    #     self.assertIn("Social Media", isi_html)
    #     self.assertIn("WELCOME", isi_html)
    #     self.assertIn("Nama", isi_html)
    #     self.assertIn("NPM", isi_html)
    #     self.assertIn("Angkatan", isi_html)
    #     self.assertIn("Tempat Kuliah", isi_html)
    #     self.assertIn("Hobi", isi_html)
    #     self.assertIn("Salvian Athallahrif Fadhil", isi_html)
    #     self.assertIn("1906398843", isi_html)
    #     self.assertIn("Maung 2019", isi_html)
    #     self.assertIn("Universitas Indonesia", isi_html)
    #     self.assertIn("Nonton youtube, olahraga, main game", isi_html)

            
          
'''
class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
'''

class TestFormHTML(TestCase):
    def test_root_help_exists(self):
        response = self.client.get('/help')
        self.assertEqual(response.status_code, 200)

    def test_template_help(self):
         response = self.client.get('/help')
         self.assertTemplateUsed(response, 'base.html')

    # def test_view_help(self):
    #     response = self.client.get('/help')
    #     isi_html = response.content.decode('utf8')

    #     self.assertIn("S", isi_html)
    #     self.assertIn("ALVI", isi_html)
    #     self.assertIn("AN", isi_html)
    #     self.assertIn("Back", isi_html)
    #     self.assertIn("Mohon Maaf", isi_html)
    #     self.assertIn("Belum Tersedia", isi_html)
    #     self.assertIn("Masukan data mata kuliah", isi_html)
    #     self.assertIn("masukan matkul",isi_html)
    #     self.assertIn("masukan dosen",isi_html)
    #     self.assertIn("masukan sks",isi_html)
    #     self.assertIn("Submit",isi_html)

    def test_model_matkul(self):
        Matkul.objects.create(name = "PPW", dosen = "pa Galdhi", sks = 3)
        jumlah_data = Matkul.objects.all().count()
        self.assertEqual(jumlah_data,1)

    