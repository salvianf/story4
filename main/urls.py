from django.urls import path
from .views import savematkul , readmatkul , deletematkul, hasilcari

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('help',views.help, name='help'),
    path('home',views.home, name='home'),
    path('cari',views.cari, name='cari'),
    path('delete',views.delete, name='delete'),
    path('savematkul', views.savematkul),
    path('readmatkul', readmatkul),
    path('deletematkul', deletematkul),
    path('data/', views.hasilcari),
    path('register/', views.registerPage, name='register'),
    path('login/', views.loginPage, name='login'),
     path('logout/', views.logoutUser, name='logout'),
    
    
]
