from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Input_Form , Delete_Form, CreateUserForm
from .models import Matkul , Post 
from django.http import JsonResponse

from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

import json
import requests


def home(request):
    return render(request, 'main/home.html')

# @login_required(login_url='main:login')
def help(request):
    response = {'input_form' : Input_Form}
    return render(request, 'main/help_extention.html', response)

def cari(request):
    
    return render(request, 'main/cari_buku.html')

def savematkul(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/readmatkul')
    else:
        return HttpResponseRedirect('/home')

def readmatkul(request):
    matkuls = Matkul.objects.all()   
    response = {'matkuls' : matkuls,}
    return render(request, 'main/home.html' , response)

# @login_required(login_url='main:login')
def delete(request):
    response = {'delete_form' : Delete_Form}
    return render(request, 'main/delete.html', response)


def deletematkul(request):
    form = Delete_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        Matkul.objects.filter(name=form).delete()
        return HttpResponseRedirect('/readmatkul')
    else:
        return HttpResponseRedirect('/home')

def hasilcari(request):
    arg= request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url_tujuan)

    data = json.loads(r.content)
    return JsonResponse(data, safe=False)

def registerPage(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created (name : ' + user + ')')
            return redirect('main:login')

    response = {'form' : form}
    return render(request, 'main/register.html', response)

def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('main:home')
        else:
            messages.info(request, 'username OR password in incorrect')
           

    response = {}
    return render(request, 'main/login.html', response)

def logoutUser(request):
    logout(request)
    return redirect('main:login')